package com.example.calculatorapp;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    TextView expressionField;
    Button button0, button1, button2, button3, button4, button5, button6, button7, button8, button9, buttonEquals,buttonAdd, buttonSub, buttonMul, buttonDiv, buttonClear;
    Integer num1, num2;
    String operator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        expressionField = findViewById(R.id.expressionField);
        button0 = findViewById(R.id.btn0);
        button1 = findViewById(R.id.btn1);
        button2 = findViewById(R.id.btn2);
        button3 = findViewById(R.id.btn3);
        button4 = findViewById(R.id.btn4);
        button5 = findViewById(R.id.btn5);
        button6 = findViewById(R.id.btn6);
        button7 = findViewById(R.id.btn7);
        button8 = findViewById(R.id.btn8);
        button9 = findViewById(R.id.btn9);
        buttonEquals = findViewById(R.id.btnEquals);
        buttonAdd = findViewById(R.id.btnAdd);
        buttonSub = findViewById(R.id.btnSub);
        buttonMul = findViewById(R.id.btnMul);
        buttonDiv = findViewById(R.id.btnDivide);
        buttonClear = findViewById(R.id.btnClear);

        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandleNumberButtonClicked(0);
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandleNumberButtonClicked(1);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandleNumberButtonClicked(2);
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandleNumberButtonClicked(3);
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandleNumberButtonClicked(4);
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandleNumberButtonClicked(5);
            }
        });
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandleNumberButtonClicked(6);
            }
        });
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandleNumberButtonClicked(7);
            }
        });
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandleNumberButtonClicked(8);
            }
        });
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandleNumberButtonClicked(9);
            }
        });
        buttonEquals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandleEqualsButtonClicked(operator);
            }
        });
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandleOperatorButtonClicked("+");
            }
        });
        buttonSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandleOperatorButtonClicked("-");
            }
        });
        buttonMul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandleOperatorButtonClicked("*");
            }
        });
        buttonDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandleOperatorButtonClicked("/");
            }
        });
        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandleClearButtonClicked();
            }
        });


    }
    void HandleButtonClicked(String pressedKey,boolean clearText){
        String priorText = expressionField.getText().toString();
        if (clearText){
            priorText = "";
        }
        String newText = priorText + pressedKey;
        expressionField.setText(newText);
    }
    void HandleNumberButtonClicked(Integer number){
        if (num1 != null && num2 != null){
            HandleButtonClicked(Integer.toString(number),true);
        }
        else if (num1 == null){
            HandleButtonClicked(Integer.toString(number),false);
            num1 = number;
        }
        else{
            HandleButtonClicked(Integer.toString(number),false);
            num2 = number;
        }

    }
    void HandleClearButtonClicked(){
        expressionField.setText("");
        num1 = null;
        num2 = null;
    }
    void HandleOperatorButtonClicked(String operator){
        HandleButtonClicked(operator,false);
        this.operator = operator;
    }
    void HandleEqualsButtonClicked(String operator){
        int result = 0;
        switch (operator){
            case "+":
                result = num1 + num2;
                break;
            case "-":
                result = num1 - num2;
                break;
            case "*":
                result = num1 * num2;
                break;
            case "/":
                result = (num2 == 0) ? -1 : num1 / num2;
                break;
        }
        expressionField.setText(Integer.toString(result));
        num1 = null;
        num2 = null;

    }
}